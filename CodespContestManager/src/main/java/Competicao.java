import java.util.ArrayList;
import java.util.Date;

public class Competicao {

	private Long id;
	// tipo = privada ou publica
	private boolean tipo;
	private String titulo;
	private Date data;
	private double duracao;
	private ArrayList<Usuario> juizes;
	private ArrayList<Usuario> competidores;
	
	// relacionamento
	private Usuario admin;

	//relacionamento
	
	
	public Submissao correcaoAuto(Submissao s) {

		return s;

	}
	
	protected Long getId() {
		return id;
	}

	protected void setId(Long id) {
		this.id = id;
	}

	protected boolean isTipo() {
		return tipo;
	}

	protected void setTipo(boolean tipo) {
		this.tipo = tipo;
	}

	public Usuario getAdmin() {
		return admin;
	}

	public void setAdmin(Usuario admin) {
		this.admin = admin;
	}
	
	protected String getTitulo() {
		return titulo;
	}

	protected void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	protected Date getData() {
		return data;
	}

	protected void setData(Date data) {
		this.data = data;
	}

	protected double getDuracao() {
		return duracao;
	}

	protected void setDuracao(double duracao) {
		this.duracao = duracao;
	}

	public ArrayList<Usuario> getJuizes() {
		return juizes;
	}

	public void setJuizes(ArrayList<Usuario> juizes) {
		this.juizes = juizes;
	}

	public ArrayList<Usuario> getCompetidores() {
		return competidores;
	}

	public void setCompetidores(ArrayList<Usuario> competidores) {
		this.competidores = competidores;
	}
	

}
