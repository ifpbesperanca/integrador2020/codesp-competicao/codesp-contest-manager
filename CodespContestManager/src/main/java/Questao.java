import java.io.File;

public class Questao {
	
	private Long id;
	private String titulo;
	private String texto;
	private File input;
	private File output;
	private int valor;
	
	protected Long getId() {
		return id;
	}
	protected void setId(Long id) {
		this.id = id;
	}
	protected String getTitulo() {
		return titulo;
	}
	protected void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	protected String getTexto() {
		return texto;
	}
	protected void setTexto(String texto) {
		this.texto = texto;
	}
	protected File getInput() {
		return input;
	}
	protected void setInput(File input) {
		this.input = input;
	}
	protected File getOutput() {
		return output;
	}
	protected void setOutput(File output) {
		this.output = output;
	}
	protected int getValor() {
		return valor;
	}
	protected void setValor(int valor) {
		this.valor = valor;
	}
	
	

}
