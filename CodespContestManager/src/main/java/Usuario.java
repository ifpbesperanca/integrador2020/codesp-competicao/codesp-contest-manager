import java.io.File;
import java.util.Date;
import java.util.ArrayList;
import java.util.Scanner;

public class Usuario {

	private Long id;
	private String nome;
	private String email;
	private String senha;

	// Métodos do admin da competição

	public Competicao criarCompeticao(boolean tipo, String titulo, Date data, double duracao, ArrayList<Usuario> juizes,
			ArrayList<Usuario> competidores) {

		Competicao c = new Competicao();

		c.setTipo(tipo);
		c.setTitulo(titulo);
		c.setData(data);
		c.setDuracao(duracao);
		c.setJuizes(juizes);
		c.setCompetidores(competidores);
		c.setAdmin(this);
		return c;

	}

	public boolean addCompetidor() {

		return true;
	}

	public boolean removerCompetidor() {

		return true;

	}

	public boolean addJuiz() {

		return true;
	}

	public boolean removerJuiz() {

		return true;

	}

	// Métodos do competidor

	public boolean entrarCompeticao(Competicao c) {

		if (c.isTipo()) {

			c.getCompetidores().add(this);
			return true;

		}

		return false;
	}

	public Submissao enviarSubmissao(Questao q, File arquivo, Competicao c) {

		for (Usuario co : c.getCompetidores()) {

			if (co.getEmail() == this.getEmail()) {

				Submissao s = new Submissao();
				s.setQuestao(q);
				s.setArquivo(arquivo);
				s.setCompeticao(c); 

				return s;
			}

		}
		return null;

	}
 
	// Métodos do Juiz

	public Submissao avaliarSubmissao(Submissao s) {

		Scanner xx = new Scanner(System.in);

		for (Usuario ju : s.getCompeticao().getJuizes()) {

			if (ju.getEmail() == this.getEmail()) {

				System.out.println("Você aceita este resultado?" + s.getResultado());
				String validacao = xx.nextLine();

				if (validacao == "sim") {

					return s;

				} else {

					System.out.println("Escolha o resultado:");
					for (int i = 0; i < Erros.values().length; i++) {
						
						System.out.println(i + " - " + Erros.values()[i]);
						
					}
					
					;
					s.setResultado(Erros.values()[xx.nextInt()] + "");

					return s;
				}

			}

		}
		return null;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

}
