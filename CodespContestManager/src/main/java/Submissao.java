import java.io.File;
import java.util.Date;

public class Submissao {
	
	private Long id;
	private Date horario;
	private String resultado;
	private Questao questao;
	private File arquivo;

	// relacionamento
	private Competicao competicao;
	
	protected Long getId() {
		return id;
	}
	protected void setId(Long id) {
		this.id = id;
	}
	protected Date getHorario() {
		return horario;
	}
	protected void setHorario(Date horario) {
		this.horario = horario;
	}
	protected String getResultado() {
		return resultado;
	}
	protected void setResultado(String resultado) {
		this.resultado = resultado;
	}
	protected Questao getQuestao() {
		return questao;
	}
	protected void setQuestao(Questao questao) {
		this.questao = questao;
	}
	public File getArquivo() {
		return arquivo;
	}
	public void setArquivo(File arquivo) {
		this.arquivo = arquivo;
	}
	public Competicao getCompeticao() {
		return competicao;
	}
	public void setCompeticao(Competicao competicao) {
		this.competicao = competicao;
	}
	
	

}
